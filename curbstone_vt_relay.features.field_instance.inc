<?php
/**
 * @file
 * curbstone_vt_relay.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function curbstone_vt_relay_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'commerce_order-commerce_order-field_curbstone_customer_id'
  $field_instances['commerce_order-commerce_order-field_curbstone_customer_id'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => -1,
      ),
    ),
    'entity_type' => 'commerce_order',
    'fences_wrapper' => 'div',
    'field_name' => 'field_curbstone_customer_id',
    'label' => 'Curbstone Customer ID',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 32,
      ),
      'type' => 'text_textfield',
      'weight' => 9,
    ),
  );

  // Exported field_instance: 'commerce_order-commerce_order-field_curbstone_order_id'
  $field_instances['commerce_order-commerce_order-field_curbstone_order_id'] = array(
    'bundle' => 'commerce_order',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'commerce_order',
    'fences_wrapper' => 'div',
    'field_name' => 'field_curbstone_order_id',
    'label' => 'Curbstone Order ID',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 32,
      ),
      'type' => 'text_textfield',
      'weight' => 10,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Curbstone Customer ID');
  t('Curbstone Order ID');

  return $field_instances;
}
