<?php
/**
 * @file
 * curbstone_vt_relay.features.field_base.inc
 */

/**
 * Implements hook_field_default_field_bases().
 */
function curbstone_vt_relay_field_default_field_bases() {
  $field_bases = array();

  // Exported field_base: 'field_curbstone_customer_id'
  $field_bases['field_curbstone_customer_id'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_curbstone_customer_id',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 32,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  // Exported field_base: 'field_curbstone_order_id'
  $field_bases['field_curbstone_order_id'] = array(
    'active' => 1,
    'cardinality' => 1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_curbstone_order_id',
    'foreign keys' => array(
      'format' => array(
        'columns' => array(
          'format' => 'format',
        ),
        'table' => 'filter_format',
      ),
    ),
    'indexes' => array(
      'format' => array(
        0 => 'format',
      ),
    ),
    'locked' => 0,
    'module' => 'text',
    'settings' => array(
      'max_length' => 32,
    ),
    'translatable' => 0,
    'type' => 'text',
  );

  return $field_bases;
}
