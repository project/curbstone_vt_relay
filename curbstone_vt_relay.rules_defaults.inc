<?php
/**
 * @file
 * curbstone_vt_relay.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function curbstone_vt_relay_default_rules_configuration() {
  $items = array();
  $items['commerce_payment_curbstone_vt_relay'] = entity_import('rules_config', '{ "commerce_payment_curbstone_vt_relay" : {
      "LABEL" : "Curbstone VT Relay",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Commerce Payment" ],
      "REQUIRES" : [ "rules", "commerce_payment" ],
      "ON" : [ "commerce_payment_methods" ],
      "IF" : [
        { "entity_has_field" : {
            "entity" : [ "commerce-order" ],
            "field" : "field_curbstone_customer_id"
          }
        },
        { "entity_has_field" : { "entity" : [ "commerce-order" ], "field" : "field_curbstone_order_id" } }
      ],
      "DO" : [
        { "data_set" : {
            "data" : [ "commerce-order:field-curbstone-customer-id" ],
            "value" : [ "site:current-user:uid" ]
          }
        },
        { "data_set" : {
            "data" : [ "commerce-order:field-curbstone-order-id" ],
            "value" : [ "commerce-order:order-id" ]
          }
        },
        { "entity_save" : { "data" : [ "commerce-order" ], "immediate" : 1 } },
        { "commerce_payment_enable_curbstone_vt_relay" : {
            "commerce_order" : [ "commerce-order" ],
            "payment_method" : { "value" : {
                "method_id" : "curbstone_vt_relay",
                "settings" : {
                  "environment" : "mock",
                  "dev_server" : "",
                  "live_server" : "",
                  "mftype" : "RA",
                  "mftype2" : "PA",
                  "curbstone_logging" : "notification"
                }
              }
            }
          }
        }
      ]
    }
  }');
  return $items;
}
