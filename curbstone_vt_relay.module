<?php

/**
 * @file
 * Implements Curbstone VT Relay in Drupal Commerce checkout.
 */

/**
 * Implements hook_menu().
 */
function curbstone_vt_relay_menu() {
  $items = array();

  $items['curbstone/mockService'] = array(
    'title' => 'Mock Curbstone Service',
    'description' => 'Dummy service.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('curbstone_vt_relay_mock_service'),
    'access arguments' => array('use curbstone mock service'),
    'file' => 'curbstone_vt_relay.pages.inc',
    'file path' => drupal_get_path('module', 'curbstone_vt_relay'),
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function curbstone_vt_relay_permission() {
  return array(
    'use curbstone mock service' => array(
      'title' => t('Use Curbstone Mock Service'),
    ),
  );
}

/**
 * Implements hook_commerce_payment_method_info().
 */
function curbstone_vt_relay_commerce_payment_method_info() {
  $payment_methods = array();

  $payment_methods['curbstone_vt_relay'] = array(
    'base' => 'curbstone_vt_relay',
    'title' => t('Curbstone VT Relay'),
    'short_title' => t('VT Relay'),
    'display_title' => t('Payment processing'),
    'description' => t('Curbstone VT Relay in Drupal Commerce checkout'),
    'terminal' => FALSE,
    'offsite' => TRUE,
    'offsite_autoredirect' => TRUE,
  );

  return $payment_methods;
}

/**
 * Returns the default settings for the Curbstone VT Relay payment method.
 */
function curbstone_vt_relay_default_settings() {
  return array(
    'environment' => 'dev',
    'mftype' => 'RA',
    'mftype2' => 'PA',
    'curbstone_logging' => 'notification',
  );
}

/**
 * Payment method callback: settings form.
 */
function curbstone_vt_relay_settings_form($settings = array()) {
  $form = array();

  // Merge default settings into the stored settings array.
  $settings = (array) $settings + curbstone_vt_relay_default_settings();

  $form['environment'] = array(
    '#type' => 'radios',
    '#title' => t('Curbstone environment'),
    '#options' => array(
      'mock' => ('Mock - use for local testing.'),
      'dev' => ('Dev - use for development.'),
      'live' => ('Live - use for processing real transactions.'),
    ),
    '#default_value' => $settings['environment'],
  );
  $form['dev_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Dev server'),
    '#default_value' => isset($settings['dev_server']) ? $settings['dev_server'] : NULL,
  );
  $form['live_server'] = array(
    '#type' => 'textfield',
    '#title' => t('Live server'),
    '#default_value' => isset($settings['live_server']) ? $settings['live_server'] : NULL,
  );
  $form['mftype'] = array(
    '#type' => 'radios',
    '#title' => t('Transaction Type'),
    '#options' => array(
      'RA' => t('RA: Perform a payment authorization.'),
      'RL' => t('RL: Perform a local void. (Not particularly useful with a payment landing page.)'),
    ),
    '#default_value' => $settings['mftype'],
  );
  $form['mftype2'] = array(
    '#type' => 'radios',
    '#title' => t('Transaction Type 2'),
    '#options' => array(
      'PA' => t('PA: Perform a pre-authorization and flag it for settlement at a later date.'),
      'SA' => t('SA: Perform a a sales transaction which is approved and flagged for settlement immediately.'),
    ),
    '#default_value' => $settings['mftype2'],
    '#states' => array(
      'visible' => array(
        ':input[name*="mftype"]' => array('value' => 'RA'),
      ),
    ),
  );
  $form['curbstone_logging'] = array(
    '#type' => 'radios',
    '#title' => t('Curbstone logging'),
    '#options' => array(
      'notification' => t('Log notifications during processing.'),
      'full_logging' => t('Log notifications with the full outbound and inbound details during validation and processing (used for debugging).'),
    ),
    '#default_value' => $settings['curbstone_logging'],
  );

  return $form;
}

/**
 * Payment method callback: Redirect form.
 *
 * A wrapper around the general use function to provide a redirect form.
 */
function curbstone_vt_relay_redirect_form($form, &$form_state, $order, $payment_method) {
  // Return an error if the enabling action's settings haven't been configured.
  $server_url = curbstone_vt_relay_server_url($payment_method['settings']);
  if (empty($server_url)) {
    drupal_set_message(t('Curbstone VT Relay environment is not configured for use.'), 'error');
    return array();
  }

  $settings = array(
    // Return to the payment redirect page for processing successful payments.
    'target' => url('checkout/' . $order->order_id . '/payment/return/' . $order->data['payment_redirect_key'], array('absolute' => TRUE)),
  );

  return curbstone_vt_relay_order_form($form, $form_state, $order, $payment_method['settings'] + $settings);
}

/**
 * Payment method callback: redirect form return validation.
 */
function curbstone_vt_relay_redirect_form_validate($order, $payment_method) {
  $return = TRUE;

  if (!empty($payment_method['settings']['curbstone_logging']) &&
    $payment_method['settings']['curbstone_logging'] == 'full_logging') {
    watchdog('curbstone_vt_relay', 'Customer returned from Curbstone with the following GET data:!data', array('!data' => '<pre>' . check_plain(print_r($_GET, TRUE)) . '</pre>'), WATCHDOG_NOTICE);
  }

  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
  $order_total = $order_wrapper->commerce_order_total->value();

  // If the user didn't submit the appropriate purchase amount to Curbstone
  // as previously recorded in Drupal, someone is doing something funny.
  if (empty($_GET['amount']) || intval((string) (floatval($_GET['amount']) * 100)) != intval($order_total['amount'])) {
    $amount = isset($_GET['amount']) ? floatval(check_url($_GET['amount'])) : '0';
    drupal_set_message(t('The purchase amount: $!amount is incorrect.', array('!amount' => $amount)), 'error');
    $return = FALSE;
  }

  // If we get an error in purchasing, let's fail the checkout.
  if (!empty($_GET['approvalText']) && $_GET['approvalText'] == 'ERROR') {
    $return = FALSE;

    if (!empty($_GET['MFRTXT'])) {
      $message = '';
      switch ($_GET['MFRTXT']) {
        case 'F':
          $message = t('Please verify that your street address has been entered correctly.');
          break;

        case 'J':
          $message = t('Please verify that your credit card expiration date has been entered correctly.');
          break;

        case 'H':
        case 'X':
          $message = t('Please verify that your credit card number has been entered correctly.');
          break;

        case 'I':
          $message = t('Please verify that your CVV2 number has been entered correctly.');
          break;

        case 'P':
          $message = t('Please verify that your postal code has been entered correctly.');
          break;

        case '8':
        case '9':
          $message = t('The credit card processing system is unavailable. Please try again later.');
          break;

        default:
          $message = t('Internal processing error. Please try again later.');
          break;
      }

      drupal_set_message($message, 'error');
    }
  }

  return $return;
}

/**
 * Builds a Payment form from an order object.
 */
function curbstone_vt_relay_order_form($form, &$form_state, $order, $settings) {
  $order_wrapper = entity_metadata_wrapper('commerce_order', $order);

  $billing_address = $order_wrapper->commerce_customer_billing->commerce_customer_address->value();

  // Build the data array that will be translated into hidden form values.
  $data = array(
    // Amount to bill.
    'amount' => commerce_currency_amount_to_decimal($order_wrapper->commerce_order_total->amount->value(), 'USD'),

    // Customer number.
    'cust' => $order_wrapper->field_curbstone_customer_id->value(),

    // Order number.
    'mfordr' => $order_wrapper->field_curbstone_order_id->value(),

     // Reference number i.e. invoice.
    'mfrefr' => $order_wrapper->field_curbstone_order_id->value(),

     // Street number.
    'mfadd1' => substr($billing_address['thoroughfare'], 0, 24),

    // Postal code.
    'mfzipc' => substr($billing_address['postal_code'], 0, 10),

    // Transaction type.
    'mftype' => $settings['mftype'],

    // Transaction type 2 for RA.
    'mftype2' => isset($settings['mftype2']) ? $settings['mftype2'] : '',

    // Return to the payment redirect page for processing successful payments.
    'target' => $settings['target'],
  );

  // Allow modules to alter parameters of the API request.
  drupal_alter('curbstone_vt_relay_order_form_data', $data, $order);

  foreach ($data as $name => $value) {
    if ($value === '0' || !empty($value)) {
      $query[$name] = trim($value);
    }
  }

  $form['#action'] = url(curbstone_vt_relay_server_url($settings), array('query' => $query, 'absolute' => TRUE));

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Proceed to payment'),
  );

  return $form;
}

/**
 * Returns the URL to the specified payment environment.
 */
function curbstone_vt_relay_server_url($settings) {
  $return = 'curbstone/mockService/';

  switch ($settings['environment']) {
    case 'dev':
    case 'live':
      $return = $settings[$settings['environment'] . '_server'];
      break;
  }

  return $return;
}
