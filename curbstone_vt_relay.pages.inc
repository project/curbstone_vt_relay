<?php

/**
 * @file
 * Page callbacks for the Curbstone VT Relay module.
 */

/**
 * Generate the mock service form.
 *
 * @ingroup forms
 */
function curbstone_vt_relay_mock_service($form, &$form_state) {
  $form = array();

  $query = drupal_get_query_parameters();

  $query = (array) $query + array(
    'mfrtrn' => 'UG',
    'mfukey' => '000000123456789',
    'mfatal' => '',
  );

  $header = array(
    'key' => array('data' => t('Key')),
    'value' => array('data' => t('Value')),
  );
  $rows = array();
  foreach ($query as $key => $value) {
    $rows[] = array(check_plain($key), check_plain($value));
  }
  $form['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
  );

  $form['#action'] = url($query['target'], array('query' => $query));

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Return'),
  );

  return $form;
}
